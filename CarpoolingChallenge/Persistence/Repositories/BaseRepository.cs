﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarpoolingChallenge.Persistence.Contexts;

namespace CarpoolingChallenge.Persistence.Repositories
{
  public abstract class BaseRepository
  {
    protected readonly CarpoolingDbContext _context;

    public BaseRepository(CarpoolingDbContext context)
    {
      _context = context;
    }
  }
}
