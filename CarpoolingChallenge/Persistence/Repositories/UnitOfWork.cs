﻿using System.Threading.Tasks;
using CarpoolingChallenge.Domain.Repositories;
using CarpoolingChallenge.Persistence.Contexts;

namespace CarpoolingChallenge.Persistence.Repositories
{
  public class UnitOfWork : IUnitOfWork
  {
    private readonly CarpoolingDbContext _context;

    public UnitOfWork(CarpoolingDbContext context)
    {
      _context = context;
    }

    public async Task CompleteAsync()
    {
      await _context.SaveChangesAsync();
    }
  }
}
