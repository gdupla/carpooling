﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using CarpoolingChallenge.Domain.Models;
using CarpoolingChallenge.Domain.Repositories;
using CarpoolingChallenge.Persistence.Contexts;

namespace CarpoolingChallenge.Persistence.Repositories
{
  public class CarpoolingRepository : BaseRepository, ICarpoolingRepository
  {
    public CarpoolingRepository(CarpoolingDbContext context) : base(context)
    {
    }

    public async Task<IEnumerable<Car>> ListCarsAsync()
    {
      return await _context.Cars.OrderBy(x => x.Seats).ToListAsync();
    }

    public async Task AddCarAsync(Car car)
    {
      await _context.Cars.AddAsync(car);
    }

    public async Task AddListCarAsync(IEnumerable<Car> cars)
    {
      foreach (Car car in _context.Cars)
        _context.Cars.Remove(car);

      foreach (Journey journey in _context.Journeys)
        _context.Journeys.Remove(journey);

      await _context.Cars.AddRangeAsync(cars);      
    }

    public async Task<Car> FindCarByIdAsync(long id)
    {
      return await _context.Cars.FindAsync(id);
    }

    public void UpdateCar(Car car)
    {
      _context.Cars.Update(car);
    }

    public async Task<IDictionary<long, Journey>> DictionaryJourneysAsync() {
      return await _context.Journeys.ToDictionaryAsync(p => p.Id, p => p);
    }

    public async Task AddJourneyAsync(Journey journey)
    {
      await _context.Journeys.AddAsync(journey);
    }

    public async Task<Journey> FindJourneyByIdAsync(long id)
    {
      return await _context.Journeys.FindAsync(id);
    }

    public void DeleteJourney(Journey journey)
    {
      _context.Journeys.Remove(journey);
    }
  }
}
