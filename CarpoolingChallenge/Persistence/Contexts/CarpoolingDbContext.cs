﻿using CarpoolingChallenge.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace CarpoolingChallenge.Persistence.Contexts
{
  public class CarpoolingDbContext : DbContext
  {
    public DbSet<Car> Cars { get; set; }

    public DbSet<Journey> Journeys { get; set; }

    public CarpoolingDbContext(DbContextOptions<CarpoolingDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
      base.OnModelCreating(builder);

      builder.Entity<Car>().ToTable("Cars");
      builder.Entity<Car>().HasKey(p => p.Id);
      builder.Entity<Car>().Property(p => p.Id).IsRequired();
      builder.Entity<Car>().Property(p => p.Seats).IsRequired();
      //builder.Entity<Car>().HasMany(p => p.Journeys).WithOne(p => p.Car).HasForeignKey(p => p.CarId);

      builder.Entity<Journey>().ToTable("Journeys");
      builder.Entity<Journey>().HasKey(p => p.Id);
      builder.Entity<Journey>().Property(p => p.Id).IsRequired();
      builder.Entity<Journey>().Property(p => p.People).IsRequired();
    }
  }
}