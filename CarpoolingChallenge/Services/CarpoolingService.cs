﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CarpoolingChallenge.Domain.Models;
using CarpoolingChallenge.Domain.Repositories;
using CarpoolingChallenge.Domain.Services;
using CarpoolingChallenge.Domain.Services.Communication;

namespace CarpoolingChallenge.Services
{
  public class CarpoolingService : ICarpoolingService
  {
    private readonly ICarpoolingRepository _carpoolingRepository;
    private readonly IUnitOfWork _unitOfWork;

    public CarpoolingService(ICarpoolingRepository carpoolingRepository, IUnitOfWork unitOfWork)
    {
      _carpoolingRepository = carpoolingRepository;
      _unitOfWork = unitOfWork;
    }

    public async Task<Car> FindCarByIdAsync(long id)
    {
      return await _carpoolingRepository.FindCarByIdAsync(id);
    }

    public async Task<IEnumerable<Car>> ListCarsAsync()
    {
      return await _carpoolingRepository.ListCarsAsync();
    }

    public async Task<SaveCarResponse> AddCarAsync(Car car)
    {
      try
      {
        await _carpoolingRepository.AddCarAsync(car);
        await _unitOfWork.CompleteAsync();

        return new SaveCarResponse(car);
      }
      catch (Exception ex)
      {
        return new SaveCarResponse($"An error occurred when saving the car: {ex.Message}");
      }
    }

    public async Task<SaveCarResponse> AddListCarAsync(IEnumerable<Car> cars)
    {
      try
      {
        await _carpoolingRepository.AddListCarAsync(cars);
        await _unitOfWork.CompleteAsync();

        return new SaveCarResponse();
      }
      catch (Exception ex)
      {
        return new SaveCarResponse($"An error occurred when saving the new list of cars: {ex.Message}");
      }
    }

    public async Task<SaveCarResponse> UpdateCarAsync(long id, Car car)
    {
      var existingCar = await _carpoolingRepository.FindCarByIdAsync(id);

      if (existingCar == null)
        return new SaveCarResponse("Car not found.");

      existingCar.Seats = car.Seats;

      try
      {
        _carpoolingRepository.UpdateCar(existingCar);
        await _unitOfWork.CompleteAsync();

        return new SaveCarResponse(existingCar);
      }
      catch (Exception ex)
      {
        return new SaveCarResponse($"An error occurred when updating the car: {ex.Message}");
      }
    }

    public async Task<Journey> FindJourneyByIdAsync(long id)
    {
      return await _carpoolingRepository.FindJourneyByIdAsync(id);
    }

    public async Task<IDictionary<long, Journey>> DictionaryJourneysAsync()
    {
      return await _carpoolingRepository.DictionaryJourneysAsync();
    }

    public async Task<SaveJourneyResponse> AddJourneyAsync(Journey journey)
    {
      try
      {
        await _carpoolingRepository.AddJourneyAsync(journey);
        await _unitOfWork.CompleteAsync();

        return new SaveJourneyResponse(journey);
      }
      catch (Exception ex)
      {
        return new SaveJourneyResponse($"An error occurred when saving the journey: {ex.Message}");
      }
    }

    public async Task<SaveJourneyResponse> DeleteJourneyAsync(long id)
    {
      var existingJourney = await _carpoolingRepository.FindJourneyByIdAsync(id);

      if (existingJourney == null)
        return new SaveJourneyResponse("Journey not found.");

      try
      {
        _carpoolingRepository.DeleteJourney(existingJourney);
        await _unitOfWork.CompleteAsync();

        return new SaveJourneyResponse(existingJourney);
      }
      catch (Exception ex)
      {
        return new SaveJourneyResponse($"An error occurred when updating the journey: {ex.Message}");
      }
    }
  }
}
