﻿using AutoMapper;
using CarpoolingChallenge.Domain.Models;
using CarpoolingChallenge.Resources;

namespace CarpoolingChallenge.Mapping
{
  public class ModelToResourceProfile : Profile
  {
    public ModelToResourceProfile()
    {
      CreateMap<Car, CarResource>();

      CreateMap<SaveCarResource, Car>();
      
      CreateMap<Journey, JourneyResource>();

      CreateMap<SaveJourneyResource, Journey>();
    }
  }
}
