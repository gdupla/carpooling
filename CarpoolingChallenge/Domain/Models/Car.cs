﻿using System.Collections.Generic;

namespace CarpoolingChallenge.Domain.Models
{
  public class Car
  {
    public long Id { get; set; }
    public int Seats { get; set; }

    //public IList<Journey> Journeys { get; set; } = new List<Journey>();
  }
}