﻿namespace CarpoolingChallenge.Domain.Models
{
  public class Journey
  {
    public long Id { get; set; }
    public int People { get; set; }

    public long CarId { get; set; }
    public Car Car { get; set; }
  }
}