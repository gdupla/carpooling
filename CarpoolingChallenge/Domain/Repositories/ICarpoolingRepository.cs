﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CarpoolingChallenge.Domain.Models;

namespace CarpoolingChallenge.Domain.Repositories
{
  public interface ICarpoolingRepository
  {
    Task<IEnumerable<Car>> ListCarsAsync();

    Task AddCarAsync(Car car);

    Task AddListCarAsync(IEnumerable<Car> cars);

    Task<Car> FindCarByIdAsync(long id);

    void UpdateCar(Car car);

    Task<IDictionary<long, Journey>> DictionaryJourneysAsync();

    Task AddJourneyAsync(Journey Journey);

    Task<Journey> FindJourneyByIdAsync(long id);

    void DeleteJourney(Journey car);
  }
}
