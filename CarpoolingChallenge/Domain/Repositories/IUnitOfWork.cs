﻿using System.Threading.Tasks;

namespace CarpoolingChallenge.Domain.Repositories
{
  public interface IUnitOfWork
  {
    Task CompleteAsync();
  }
}
