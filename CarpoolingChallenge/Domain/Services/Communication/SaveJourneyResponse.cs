﻿using CarpoolingChallenge.Domain.Models;

namespace CarpoolingChallenge.Domain.Services.Communication
{
  public class SaveJourneyResponse : BaseResponse
  {
    public Journey Journey { get; private set; }

    private SaveJourneyResponse(bool success, string message, Journey journey) : base(success, message)
    {
      Journey = journey;
    }

    public SaveJourneyResponse(Journey journey) : this(true, string.Empty, journey)
    { }

    public SaveJourneyResponse(string message) : this(false, message, null)
    { }
  }
}
