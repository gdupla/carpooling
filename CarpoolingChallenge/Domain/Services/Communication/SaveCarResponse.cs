﻿using CarpoolingChallenge.Domain.Models;

namespace CarpoolingChallenge.Domain.Services.Communication
{
  public class SaveCarResponse : BaseResponse
  {
    public Car Car { get; private set; }

    private SaveCarResponse(bool success, string message, Car car) : base(success, message)
    {
      Car = car;
    }

    public SaveCarResponse() : this(true, string.Empty, null)
    { }

    public SaveCarResponse(Car car) : this(true, string.Empty, car)
    { }

    public SaveCarResponse(string message) : this(false, message, null)
    { }
  }
}
