﻿using CarpoolingChallenge.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using CarpoolingChallenge.Domain.Services.Communication;

namespace CarpoolingChallenge.Domain.Services
{
  public interface ICarpoolingService
  {
    Task<Car> FindCarByIdAsync(long id);

    Task<IEnumerable<Car>> ListCarsAsync();

    Task<SaveCarResponse> AddCarAsync(Car car);

    Task<SaveCarResponse> AddListCarAsync(IEnumerable<Car> cars);
    
    Task<SaveCarResponse> UpdateCarAsync(long id, Car car);

    Task<Journey> FindJourneyByIdAsync(long id);

    Task<IDictionary<long, Journey>> DictionaryJourneysAsync();

    Task<SaveJourneyResponse> AddJourneyAsync(Journey Journey);

    Task<SaveJourneyResponse> DeleteJourneyAsync(long id);
  }
}
