﻿namespace CarpoolingChallenge.Resources
{
  public class JourneyResource
  {
    public long Id { get; set; }

    public int People { get; set; }

    public long CarId { get; set; }
  }
}
