﻿using System.ComponentModel.DataAnnotations;

namespace CarpoolingChallenge.Resources
{
  public class SaveJourneyResource
  {
    [Required]
    public long Id { get; set; }

    [Required]
    [Range(1, 6, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
    public int People { get; set; }
  }
}
