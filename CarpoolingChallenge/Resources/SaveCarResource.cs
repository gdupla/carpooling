﻿using System.ComponentModel.DataAnnotations;

namespace CarpoolingChallenge.Resources
{
  public class SaveCarResource
  {
    [Required]
    public long Id { get; set; }

    [Required]
    [Range(4, 6, ErrorMessage = "Value for {0} must be between {1} and {2}.")]
    public int Seats { get; set; }
  }
}
