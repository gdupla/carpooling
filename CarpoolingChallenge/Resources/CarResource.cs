﻿namespace CarpoolingChallenge.Resources
{
  public class CarResource
  {
    public long Id { get; set; }

    public int Seats { get; set; }
  }
}
