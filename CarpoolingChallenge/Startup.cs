﻿using CarpoolingChallenge.Domain.Repositories;
using CarpoolingChallenge.Domain.Services;
using CarpoolingChallenge.Persistence.Contexts;
using CarpoolingChallenge.Persistence.Repositories;
using CarpoolingChallenge.Services;
using CarpoolingChallenge.Resources;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;

namespace CarpoolingChallenge
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
      services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

      services.AddDbContext<CarpoolingDbContext>(options =>
                options.UseInMemoryDatabase("carpooling-in-memory"));

      services.AddScoped<ICarpoolingRepository, CarpoolingRepository>();

      services.AddScoped<ICarpoolingService, CarpoolingService>();

      services.AddAutoMapper();

      services.AddScoped<IUnitOfWork, UnitOfWork>();
    }

    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.UseHsts();
      }

      app.UseHttpsRedirection();
      app.UseMvc();
    }
  }
}
