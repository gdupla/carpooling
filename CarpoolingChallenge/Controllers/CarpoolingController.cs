﻿using AutoMapper;
using CarpoolingChallenge.Domain.Models;
using CarpoolingChallenge.Domain.Services;
using CarpoolingChallenge.Extensions;
using CarpoolingChallenge.Resources;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace CarpoolingChallenge.Controllers
{
  public class CarpoolingController : Controller
  {
    private readonly ICarpoolingService _carpoolingService;

    private readonly IMapper _mapper;

    public CarpoolingController(ICarpoolingService carpoolingService, IMapper mapper)
    {
      _carpoolingService = carpoolingService;
      _mapper = mapper;
    }

    // GET: /status
    [HttpGet("status")]
    public IActionResult GetStatus()
    {
      return Ok();
    }

    // GET: /cars
    [HttpGet("cars")]
    public async Task<IEnumerable<CarResource>> GetAllCarsAsync()
    {
      var cars = await _carpoolingService.ListCarsAsync();
      var resources = _mapper.Map<IEnumerable<Car>, IEnumerable<CarResource>>(cars);
      return resources;
    }

    // PUT: /cars
    [HttpPut("cars")]
    public async Task<IActionResult> PutCars([FromBody] IEnumerable<SaveCarResource> carResources)
    {
      if (!ModelState.IsValid)
        return BadRequest(ModelState.GetErrorMessages());

      var cars = _mapper.Map<IEnumerable<SaveCarResource>, IEnumerable<Car>>(carResources);
      var result = await _carpoolingService.AddListCarAsync(cars);

      if (!result.Success)
        return BadRequest(result.Message);

      return Ok();
    }

    // GET: /journeys
    [HttpGet("journeys")]
    public async Task<IEnumerable<JourneyResource>> GetAllJourneysAsync()
    {
      var journeys = await _carpoolingService.DictionaryJourneysAsync();
      var resources = _mapper.Map<IEnumerable<Journey>, IEnumerable<JourneyResource>>(journeys.Values);
      return resources;
    }

    // POST: /journey
    [HttpPost("journey")]
    public async Task<IActionResult> PostJourney([FromBody] SaveJourneyResource resource)
    {
      if (!ModelState.IsValid)
        return BadRequest(ModelState.GetErrorMessages());

      var journey = _mapper.Map<SaveJourneyResource, Journey>(resource);

      var cars = await _carpoolingService.ListCarsAsync();

      bool accepted = false;
      foreach (Car car in cars)
      {
        if (car.Seats >= journey.People)
        {
          journey.CarId = car.Id;
          journey.Car = car;
          car.Seats = car.Seats - journey.People;
          await _carpoolingService.UpdateCarAsync(car.Id, car);
          accepted = true;
          break;
        }
      }

      var result = await _carpoolingService.AddJourneyAsync(journey);
      if (!result.Success)
        return BadRequest(result.Message);

      if (accepted)
        return Accepted();
      else      
        return Ok();      
    }

    [HttpPost("dropoff/ID={id}")]
    public async Task<IActionResult> PostDropoff(long id)
    {
      if (!ModelState.IsValid)
        return BadRequest(ModelState.GetErrorMessages());

      var existingJourney = await _carpoolingService.FindJourneyByIdAsync(id);
      if (existingJourney == null)
        return NotFound();
      else if (existingJourney.CarId == 0)
        return NoContent();
      else
      {
        var existingCar = await _carpoolingService.FindCarByIdAsync(existingJourney.CarId);
        if (existingCar == null)
          return BadRequest("The car of the journey is not valid");
        else
        {
          existingCar.Seats = existingCar.Seats + existingJourney.People;
          await _carpoolingService.UpdateCarAsync(existingCar.Id, existingCar);

          await _carpoolingService.DeleteJourneyAsync(existingJourney.Id);

          return Ok();
        }
      }
    }

    [HttpPost("locate/ID={id}")]
    public async Task<IActionResult> PostLocate(long id)
    {
      if (!ModelState.IsValid)
        return BadRequest(ModelState.GetErrorMessages());

      var existingJourney = await _carpoolingService.FindJourneyByIdAsync(id);

      if (existingJourney == null)
          return NotFound();   
      else if (existingJourney.CarId == 0)
        return NoContent();
      else
        return Ok(existingJourney.CarId);
    }
  }
}
